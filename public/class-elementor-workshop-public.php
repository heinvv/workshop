<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https//abanganimedia.co.za/elementor/workshop
 * @since      1.0.0
 *
 * @package    Elementor_Workshop
 * @subpackage Elementor_Workshop/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Elementor_Workshop
 * @subpackage Elementor_Workshop/public
 * @author     Abangani Media <info@abanganimedia.co.za>
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class Elementor_Workshop_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $elementor_workshop    The ID of this plugin.
	 */
	private $elementor_workshop;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $elementor_workshop       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $elementor_workshop, $version ) {

		$this->elementor_workshop = $elementor_workshop;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Elementor_Workshop_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Elementor_Workshop_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->elementor_workshop, plugin_dir_url( __FILE__ ) . 'css/elementor-workshop-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Elementor_Workshop_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Elementor_Workshop_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->elementor_workshop, plugin_dir_url( __FILE__ ) . 'js/elementor-workshop-public.js', array( 'jquery' ), $this->version, false );

	}

}
