<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https//abanganimedia.co.za/elementor/workshop
 * @since      1.0.0
 *
 * @package    Elementor_Workshop
 * @subpackage Elementor_Workshop/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Elementor_Workshop
 * @subpackage Elementor_Workshop/includes
 * @author     Abangani Media <info@abanganimedia.co.za>
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class Elementor_Workshop_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
