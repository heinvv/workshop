<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https//abanganimedia.co.za/elementor/workshop
 * @since      1.0.0
 *
 * @package    Elementor_Workshop
 * @subpackage Elementor_Workshop/admin/partials
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}
?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
