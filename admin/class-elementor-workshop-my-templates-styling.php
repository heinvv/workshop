<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class Elementor_Workshop_My_Templates_Styling {

	public function __construct() {

        add_action('elementor/editor/footer', [ $this, 'template_styling' ], 10, 99 );

	}

    public function template_styling() {
        ?>
        <script type="text/javascript">        
            <?php
                require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/js/elementor-workshop-remove-template.js';
            ?>
        </script>
        <style>
            <?php
                require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/css/elementor-workshop-template.css';
            ?>
        </style>
        <?php
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/elementor-workshop-new-template.php';
    }
}